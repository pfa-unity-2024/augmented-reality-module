#!/bin/bash

# Function to check if a file is a C# script
is_csharp() {
    [[ "$1" =~ \.cs$ ]]
}

# Function to check if a file is a Unity script
is_unity_script() {
    is_csharp "$1" && grep -q 'using UnityEngine;' "$1"
}

# Check each staged file
git diff --cached --name-only --diff-filter=ACM | while read -r file; do
    # Check if the file is a Unity script
    if is_unity_script "$file"; then
        # Format the script using clang-format
        clang-format -style=file -i "$file"
        # Add the formatted script to the index
        git add "$file"
    fi
done

exit 0
