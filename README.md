# Augmented-Reality-Module

## Description

Ce projet PFA est un package Unity pour utiliser une scène de Réalité Augmentée desinée initialement au client "SpaceReset".
Il fournit un système pour faire apparaître automatiquement un objet 3D sur un marqueur, les deux objets étant interchangeables.
On peut interagir avec l'objet en touchant son écran en sa direction pour faire apparaître un panel d'information sur ce dernier.

## Installation

Vous pouvez télécharger les .unitypackage disponibles sur les releases du projet et les installer sur vos propres projets Unity.
Autrement, vous pouvez tester directement l'application en installant les .apk sur vos appareils Android.

## Auteurs

BANIDE Christian
DESCOMPS Théo
DUCAMP Simon
DESBOIS-RENAUDIN Méo
MORENO CARPIO Kenzo
PIETTE Camille

## License

Libre d'utilisation, commerciale ou non.

## Project status

Projet incomplet en arrêt définitif.
